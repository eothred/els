# README #

Yngve's copy of ELS (ESS Linac Simulator). This is **not** the official location to obtain this code! Please contact E. Laface for the most up to date version.

### What changes have I done?
 * Makefile/compile script replaced by CMake
 * Moved some files around