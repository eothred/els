#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_multifit.h>

#include "general.h"
#include "fit.h"

int main(int argc, char **argv)
{
    cavity cav;
    int i;

    if (readFile(argv[1],&cav)==-1)
        return -1;

    Fit(&cav);

    for (i=0;i<cav.NumberOfElements; i++)
        printf("%f %f %f %f\n", cav.Position[i], cav.Field[i], cav.AnalyticField[i], cav.Field[i]-cav.AnalyticField[i]);

    return 0;
}
