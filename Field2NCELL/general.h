typedef struct MyExponential
{
    double zero;
    double exponent;
    double sigma;
    double amplitude;
    int startIndex;
    int endIndex;
} exponential;

typedef struct MySinusoidal
{
    double frequency;
    double phase;
    double * amplitude;
    int startIndex;
    int endIndex;
    int numberOfHarmonics;
} sinusoidal;

typedef struct MyCavity
{
    double * Field;
    double * AnalyticField;
    double * Position;
    int NumberOfElements;
    double ScaleFactor;
    double Length;
    exponential ExpLeft;
    sinusoidal CosLeft;
    sinusoidal CosCenter;
    sinusoidal CosRight;
    exponential ExpRight;
} cavity;

int readFile(char *filename, cavity * cav);
