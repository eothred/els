#define HARMONICS 3

int sign(double value);

void exponentialFitLeft(cavity * cav);

void exponentialFitRight(cavity * cav);

void sinusoidalFitLeft(cavity * cav);

void sinusoidalFitRight(cavity * cav);

void sinusoidalFitCenter(cavity * cav);

void Fit(cavity * cav);
