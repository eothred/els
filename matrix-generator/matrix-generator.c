/* ESS Linac Simulator - Matrix Generator
 * Copyright (C) 2013 Emanuele Laface - Emanuele.Laface@esss.se
 * Last update July 3rd 2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define speed_of_light 299792458
#define R0 1.53469825003334879e-18
#define mass 938.272046 /* Proton */
/* #define mass 938.29431 */ /* H- */
#define charge 1.602176565e-19

#define DRIFT 1
#define QUAD 2
#define EDGE 3
#define BEND 4
#define DTL_CEL 5
#define NCELLS 6
#define MULTIPOLE 7
#define FREQ 8
#define GAP 9
#define END 100

double matrix[6][6];

double relativistic_beta;
double relativistic_gamma;
double frequency;
double energy;
double aperture;

FILE *file_matrix;
FILE *file_energy;
FILE *file_geometry;

int element_counter;
double start_position, end_position;

struct tracewin_element
{
    int type;
    double param[100];
};

struct tracewin_element element;

double gamma2beta(double gamma)
{
    return sqrt(1-1/pow(gamma,2));
}

double beta2gamma(double beta)
{
    return sqrt(1/(1-pow(beta,2)));
}

double gamma2energy(double gamma)
{
    return (gamma-1)*mass;
}

double energy2gamma(double kinetic)
{
    return 1+kinetic/mass;
}

void print_matrix()
{
    int i,j;
    fprintf(file_matrix,"Element # %d\n",element_counter);
    fprintf(file_matrix,"From postion %f m to %f m\n", start_position,end_position);
    for (i=0;i<6;i++)
    {
        for(j=0;j<6;j++)
            fprintf(file_matrix,"%.10E	",matrix[i][j]);
        fprintf(file_matrix,"\n");
    }

    fprintf(file_energy,"%f %f %f %f\n",end_position,aperture*1000,gamma2energy(relativistic_gamma), frequency);
    return;
}

void matrix_multiply(double in1[6][6], double in2[6][6], double out[6][6])
{
    int i,j,k;

    for (i=0;i<6;i++)
        for(j=0;j<6;j++)
        {
            out[i][j]=0;
            for (k=0;k<6;k++)
                out[i][j]=out[i][j]+in1[i][k]*in2[k][j];
        }
    return;
}

void drift(double L, double R, double Ry)
{
    int i,j;
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    for (i=0;i<6;i++)
        matrix[i][i]=1;

    matrix[0][1]=L;
    matrix[2][3]=L;
    matrix[4][5]=L/pow(relativistic_gamma,2);
    aperture=R;
    return;
}

void drift_split(int split, double L, double R, double Ry)
{
    int i;
    if (split==0)
    {
        drift(L,R,Ry);
        end_position=start_position+L;
        print_matrix();
        element_counter++;
        start_position=end_position;
    }
    else
    {
        for (i=0;i<split;i++)
        {
            drift(L/(double)split,R,Ry);
            end_position=start_position+L/(double)split;
            print_matrix();
            element_counter++;
            start_position=end_position;
        }
    }
    return;
}

void quad(double L, double G, double R, double Phi, double G3, double G4, double G5, double G6)
{
    int i,j;
    double k;
    double relativistic_betarho;

    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    relativistic_betarho=mass*1e6/speed_of_light*relativistic_beta*relativistic_gamma;
    k=sqrt(fabs(G/relativistic_betarho));

    if (G>0)
    {
        matrix[0][0]=cos(k*L);	
        matrix[0][1]=sin(k*L)/k;
        matrix[1][0]=-k*sin(k*L);
        matrix[1][1]=cos(k*L);	

        matrix[2][2]=cosh(k*L);	
        matrix[2][3]=sinh(k*L)/k;
        matrix[3][2]=k*sinh(k*L);
        matrix[3][3]=cosh(k*L);	

        matrix[4][4]=1;
        matrix[4][5]=L/pow(relativistic_gamma,2);
        matrix[5][5]=1;
    }
    if (G<0)
    {
        matrix[0][0]=cosh(k*L);	
        matrix[0][1]=sinh(k*L)/k;
        matrix[1][0]=k*sinh(k*L);
        matrix[1][1]=cosh(k*L);	

        matrix[2][2]=cos(k*L);	
        matrix[2][3]=sin(k*L)/k;
        matrix[3][2]=-k*sin(k*L);
        matrix[3][3]=cos(k*L);	

        matrix[4][4]=1;
        matrix[4][5]=L/pow(relativistic_gamma,2);
        matrix[5][5]=1;
    }
    if (G==0)
        drift(L, R, 0);
    aperture=R;
    return;
}

void quad_split(int split, double L, double G, double R, double Phi, double G3, double G4, double G5, double G6)
{
    int i;
    if (split==0)
    {
        quad(L,G,R,Phi,G3,G4,G5,G6);
        end_position=start_position+L;
        print_matrix();
        element_counter++;
        start_position=end_position;
    }
    else
    {
        for (i=0;i<split;i++)
        {
            quad(L/(double)split,G,R,Phi,G3,G4,G5,G6);
            end_position=start_position+L/(double)split;
            print_matrix();
            element_counter++;
            start_position=end_position;
        }
    }
    return;
}

void edge(double B, double rho, double G, double K1, double K2, double R, double HV)
{
    int i,j;

    double psi;

    psi=K1*G/fabs(rho)*((1+pow(sin(B),2))/cos(B))*(1-K1*K2*G/fabs(rho)*tan(B));

    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    matrix[0][0]=1;
    matrix[1][1]=1;

    matrix[2][2]=1;
    matrix[3][3]=1;

    if (HV==0)
    {
        matrix[1][0]=tan(B)/fabs(rho);
        matrix[3][2]=-tan(B-psi)/fabs(rho);
    }
    if (HV==1)
    {
        matrix[1][0]=-tan(B-psi)/fabs(rho);
        matrix[3][2]=tan(B)/fabs(rho);
    }

    matrix[4][4]=1;
    matrix[5][5]=1;

    aperture=R;
    return;
}

void edge_split(int split, double B, double rho, double G, double K1, double K2, double R, double HV)
{
    edge(B,rho,G,K1,K2,R,HV);
    end_position=start_position;
    print_matrix();
    element_counter++;
    start_position=end_position;
    return;
}
void bend(double alfa, double rho, double N, double R, double HV)
{

    int i,j;

    double h;
    double kx;
    double ky;
    double Deltas;

    h=1/fabs(rho)*alfa/fabs(alfa);
    kx=sqrt((1-N)*pow(h,2));
    ky=sqrt(N*pow(h,2));

    Deltas=fabs(rho*alfa);

    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    if (N==0 && HV==0)
    {
        matrix[0][0]=cos(kx*Deltas);
        matrix[0][1]=sin(kx*Deltas)/kx;
        matrix[1][0]=-kx*sin(kx*Deltas);
        matrix[1][1]=cos(kx*Deltas);

        matrix[2][2]=1;
        matrix[2][3]=Deltas;
        matrix[3][3]=1;

        matrix[4][4]=1;
        matrix[4][5]=-pow(h,2)*(kx*Deltas*pow(relativistic_beta,2)-sin(kx*Deltas))/pow(kx,3)+Deltas/pow(relativistic_beta*relativistic_gamma,2)*(1-pow(h/kx,2));
        matrix[5][5]=1;

        matrix[4][0]=-h*sin(kx*Deltas)/kx;
        matrix[4][1]=-h*(1-cos(kx*Deltas))/pow(kx,2);

        matrix[0][5]=h*(1-cos(kx*Deltas))/pow(kx,2);
        matrix[1][5]=h*sin(kx*Deltas)/kx;
    }
    if (N==0 && HV==1)
    {
        matrix[0][0]=1;
        matrix[0][1]=Deltas;
        matrix[1][1]=1;

        matrix[2][2]=cos(kx*Deltas);
        matrix[2][3]=sin(kx*Deltas)/kx;
        matrix[3][2]=-kx*sin(kx*Deltas);
        matrix[3][3]=cos(kx*Deltas);

        matrix[4][4]=1;
        matrix[4][5]=-pow(h,2)*(kx*Deltas*pow(relativistic_beta,2)-sin(kx*Deltas))/pow(kx,3)+Deltas/pow(relativistic_beta*relativistic_gamma,2)*(1-pow(h/kx,2));
        matrix[5][5]=1;

        matrix[4][2]=-h*sin(kx*Deltas)/kx;
        matrix[4][3]=-h*(1-cos(kx*Deltas))/pow(kx,2);

        matrix[2][5]=h*(1-cos(kx*Deltas))/pow(kx,2);
        matrix[3][5]=h*sin(kx*Deltas)/kx;
    }
    if (N>0 && N<1 && HV==0)
    {
        matrix[0][0]=cos(kx*Deltas);
        matrix[0][1]=sin(kx*Deltas)/kx;
        matrix[1][0]=-kx*sin(kx*Deltas);
        matrix[1][1]=cos(kx*Deltas);

        matrix[2][2]=cos(ky*Deltas);
        matrix[2][3]=sin(ky*Deltas)/ky;
        matrix[3][2]=-ky*sin(ky*Deltas);
        matrix[3][3]=cos(ky*Deltas);

        matrix[4][4]=1;
        matrix[4][5]=-pow(h,2)*(kx*Deltas*pow(relativistic_beta,2)-sin(kx*Deltas))/pow(kx,3)+Deltas/pow(relativistic_beta*relativistic_gamma,2)*(1-pow(h/kx,2));
        matrix[5][5]=1;

        matrix[4][2]=-h*sin(kx*Deltas)/kx;
        matrix[4][3]=-h*(1-cos(kx*Deltas))/pow(kx,2);

        matrix[2][5]=h*(1-cos(kx*Deltas))/pow(kx,2);
        matrix[3][5]=h*sin(kx*Deltas)/kx;
    }
    aperture=R;
    return;
}

void bend_split(int split, double alfa, double rho, double N, double R, double HV)
{
    int i;
    if (split==0)
    {
        bend(alfa,rho,N,R,HV);
        end_position=start_position+fabs(rho*alfa);
        print_matrix();
        element_counter++;
        start_position=end_position;
    }
    else
    {
        for (i=0;i<split;i++)
        {
            bend(alfa/(double)split,rho,N,R,HV);
            end_position=start_position+fabs(rho*alfa)/(double)split;
            print_matrix();
            element_counter++;
            start_position=end_position;
        }
    }
    return;
}

double gap(double E0TL, double Phis, double R, double p, double betas, double Ts, double kTs, double k2Ts)
{
    int i,j;

    double gamma_start;
    double gamma_end;
    double gamma_middle;
    double gamma_avg;
    double beta_start;
    double beta_end;
    double beta_middle;
    double beta_avg;
    double DeltaPhi;
    double lambda;
    double E0TL_scaled;

    double kx;
    double ky;
    double kxy;
    double kz;

    double C;
    double T;
    double kT;
    double k;

    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    DeltaPhi=0;

    if (E0TL==0)
    {
        matrix[0][0]=1;
        matrix[1][1]=1;
        matrix[2][2]=1;
        matrix[3][3]=1;
        matrix[4][4]=1;
        matrix[5][5]=1;
    }
    else
    {
        gamma_start=relativistic_gamma;
        beta_start=relativistic_beta;
        lambda=speed_of_light/frequency;

        if (betas!=0)
        {
            gamma_middle=gamma_start+E0TL/(mass*1e6)*cos(Phis)/2;
            beta_middle=gamma2beta(gamma_middle);
            k=betas/beta_middle;
            T=Ts+kTs*(k-1)+k2Ts*pow(k-1,2)/2;
            kT=k*(kTs+k2Ts*(k-1));
            E0TL_scaled=E0TL*T/Ts;

            gamma_end=gamma_start+E0TL_scaled/(mass*1e6)*cos(Phis);
            beta_end=gamma2beta(gamma_end);
            gamma_avg=(gamma_end+gamma_start)/2;
            beta_avg=(beta_end+beta_start)/2;
            /*
               DeltaPhi=E0TL_scaled/(mass*1e6)*sin(Phis)/(pow(gamma_avg,2)*beta_avg)*(kT/T);
               */
            DeltaPhi=E0TL_scaled/(mass*1e6)*sin(Phis)/(pow(gamma_avg,3)*pow(beta_avg,2))*(kT/T);
            kxy=-M_PI*E0TL_scaled/(mass*1e6)*sin(Phis)/(pow(gamma_avg*beta_avg,2)*lambda);
            kx=1-E0TL_scaled/(2*mass*1e6)*cos(Phis)/(pow(beta_avg,2)*pow(gamma_avg,3))*(pow(gamma_avg,2)+kT/T);
            ky=1-E0TL_scaled/(2*mass*1e6)*cos(Phis)/(pow(beta_avg,2)*pow(gamma_avg,3))*(pow(gamma_avg,2)-kT/T);
            kz=2*M_PI*(E0TL_scaled/(mass*1e6))*sin(Phis)/(pow(beta_avg,2)*lambda);
        }
        else
        {
            DeltaPhi=0;
            gamma_end=gamma_start+E0TL/(mass*1e6)*cos(Phis);
            beta_end=gamma2beta(gamma_end);
            gamma_avg=(gamma_end+gamma_start)/2;
            beta_avg=(beta_end+beta_start)/2;

            kxy=-M_PI*E0TL/(mass*1e6)*sin(Phis)/(pow(gamma_avg*beta_avg,2)*lambda);
            kx=1-E0TL/(2*mass*1e6)*cos(Phis)/(pow(beta_avg,2)*gamma_avg);
            ky=kx;
            kz=2*M_PI*E0TL/(mass*1e6)*sin(Phis)/(pow(beta_avg,2)*lambda);
        }
        C=sqrt(((beta_start*gamma_start)/(beta_end*gamma_end))/(kx*ky));

        matrix[0][0]=kx*C;
        matrix[1][0]=kxy/(beta_end*gamma_end);
        matrix[1][1]=ky*C;

        matrix[2][2]=kx*C;
        matrix[3][2]=kxy/(beta_end*gamma_end);
        matrix[3][3]=ky*C;

        matrix[4][4]=1;
        matrix[5][4]=kz/(beta_end*gamma_end);
        matrix[5][5]=(beta_start*gamma_start)/(beta_end*gamma_end);

        relativistic_beta=beta_end;
        relativistic_gamma=gamma_end;
    }
    aperture=R;
    return DeltaPhi;
}

double gap_split(int split, double E0TL, double Phis, double R, double p, double betas, double Ts, double kTs, double k2Ts)
{
    double DeltaPhi;
    DeltaPhi=gap(E0TL,Phis,R,p,betas,Ts,kTs,k2Ts);
    end_position=start_position;
    print_matrix();
    element_counter++;
    start_position=end_position;
    return DeltaPhi;
}

void dtl_cell(double L, double Lq1, double Lq2, double g, double B1, double B2, double E0TL, double Phis, double R, double p, double betas, double Ts, double kTs, double k2Ts)
{
    int i,j;

    double tmp_in[6][6];
    double tmp_out[6][6];

    quad(Lq1, B1, R, 0, 0, 0, 0, 0); 
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            tmp_in[i][j]=matrix[i][j];

    drift(L/2-Lq1-g, R, 0);
    matrix_multiply(matrix,tmp_in,tmp_out);
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            tmp_in[i][j]=tmp_out[i][j];

    gap(E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
    matrix_multiply(matrix,tmp_in,tmp_out);
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            tmp_in[i][j]=tmp_out[i][j];

    drift(L/2-Lq2+g, R, 0);
    matrix_multiply(matrix,tmp_in,tmp_out);
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            tmp_in[i][j]=tmp_out[i][j];

    quad(Lq2, B2, R, 0, 0, 0, 0, 0);
    matrix_multiply(matrix,tmp_in,tmp_out);
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=tmp_out[i][j];
    return;
}

void dtl_cell_split(int split, double L, double Lq1, double Lq2, double g, double B1, double B2, double E0TL, double Phis, double R, double p, double betas, double Ts, double kTs, double k2Ts)
{
    quad_split(split, Lq1, B1, R, 0, 0, 0, 0, 0); 
    drift_split(split, L/2-Lq1-g, R, 0);
    gap_split(split, E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
    drift_split(split, L/2-Lq2+g, R, 0);
    quad_split(split, Lq2, B2, R, 0, 0, 0, 0, 0);
    return;
}

double ncells(double m, double n, double betag, double E0T, double Phis, double R, double p, double kE0Ti, double kE0To, double dzi, double dzo, double betas, double Ts, double kTs, double k2Ts, double Ti, double kTi, double k2Ti, double To, double kTo, double k2To)
{
    int i,j, cell;

    double tmp_in[6][6];
    double tmp_out[6][6];
    double Le, Ls, Lc;
    double E0TL;
    double lambda;
    double length;
    double DeltaPhi;

    lambda=speed_of_light/frequency;
    length=0;
    DeltaPhi=0;

    if (m==0)
    {
        Le=(betag*lambda)/2+dzi;
        Ls=(betag*lambda)/2-dzi;
        Lc=betag*lambda;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);

        drift(Le,R,0);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=matrix[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        for (cell=1;cell<n-1;cell++)
        {
            Le=(betag*lambda)/2;
            Phis=Phis+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=betag*lambda;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);

            drift(Le,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            DeltaPhi=gap(E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            drift(Ls,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];
        }

        Le=(betag*lambda)/2+dzo;
        Phis=Phis+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/2-dzo;
        Lc=betag*lambda;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift(Le,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=matrix[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, To, kTo, k2To);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                matrix[i][j]=tmp_out[i][j];
    }
    if (m==1)
    {
        Le=(betag*lambda)/4+dzi;
        Ls=(betag*lambda)/4-dzi;
        Lc=(betag*lambda)/2;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);
        drift(Le,R,0);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=matrix[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j]; 

        for (cell=1;cell<n-1;cell++)
        {
            Le=(betag*lambda)/4;
            Phis=Phis+M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=(betag*lambda)/2;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);
            drift(Le,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            DeltaPhi=gap(E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            drift(Ls,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];
        }

        Le=(betag*lambda)/4+dzo;
        Phis=Phis+M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/4-dzo;
        Lc=(betag*lambda)/2;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift(Le,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, To, kTo, k2To);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                matrix[i][j]=tmp_out[i][j];
    }
    if (m==2)
    {
        Le=(betag*lambda)/4+dzo+dzi;
        Ls=(betag*lambda)/2-dzi;
        Lc=3*(betag*lambda)/4;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);

        drift(Le,R,0);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=matrix[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        for (cell=1;cell<n-1;cell++)
        {
            Le=1/4*betag*lambda;
            Phis=Phis+2*M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=betag*lambda;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);

            drift(Le,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            DeltaPhi=gap(E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];

            drift(Ls,R,0);
            matrix_multiply(matrix,tmp_in,tmp_out);
            for (i=0;i<6;i++)
                for (j=0;j<6;j++)
                    tmp_in[i][j]=tmp_out[i][j];
        }

        Le=(betag*lambda)/2+dzo;
        Phis=Phis+2*M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/4-dzo;
        Lc=3*(betag*lambda)/4;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift(Le,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        DeltaPhi=gap(E0TL, Phis, R, p, betas, To, kTo, k2To);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                tmp_in[i][j]=tmp_out[i][j];

        drift(Ls,R,0);
        matrix_multiply(matrix,tmp_in,tmp_out);
        for (i=0;i<6;i++)
            for (j=0;j<6;j++)
                matrix[i][j]=tmp_out[i][j];
    }
    return length;
}

double ncells_split(int split, double m, double n, double betag, double E0T, double Phis, double R, double p, double kE0Ti, double kE0To, double dzi, double dzo, double betas, double Ts, double kTs, double k2Ts, double Ti, double kTi, double k2Ti, double To, double kTo, double k2To)
{
    int cell;

    double Le, Ls, Lc;
    double E0TL;
    double lambda;
    double length;
    double DeltaPhi;

    lambda=speed_of_light/frequency;
    length=0;
    DeltaPhi=0;

    if (m==0)
    {
        Le=(betag*lambda)/2+dzi;
        Ls=(betag*lambda)/2-dzi;
        Lc=betag*lambda;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);

        drift_split(split*Le, Le,R,0);
        DeltaPhi=gap_split(split, E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        drift_split(split*Ls, Ls,R,0);

        for (cell=1;cell<n-1;cell++)
        {
            Le=(betag*lambda)/2;
            Phis=Phis+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=betag*lambda;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);

            drift_split(split*Le, Le,R,0);
            DeltaPhi=gap_split(split, E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            drift_split(split*Ls,Ls,R,0);
        }

        Le=(betag*lambda)/2+dzo;
        Phis=Phis+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/2-dzo;
        Lc=betag*lambda;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift_split(split*Le,Le,R,0);
        DeltaPhi=gap_split(split, E0TL, Phis, R, p, betas, To, kTo, k2To);
        drift_split(split*Ls, Ls,R,0);
    }
    if (m==1)
    {
        Le=(betag*lambda)/4+dzi;
        Ls=(betag*lambda)/4-dzi;
        Lc=(betag*lambda)/2;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);
        drift_split(split*Le, Le,R,0);
        DeltaPhi=gap_split(split, E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        drift_split(split*Ls,Ls,R,0);

        for (cell=1;cell<n-1;cell++)
        {
            Le=(betag*lambda)/4;
            Phis=Phis+M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=(betag*lambda)/2;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);
            drift_split(split*Le,Le,R,0);
            DeltaPhi=gap_split(split,E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            drift_split(split*Ls,Ls,R,0);
        }

        Le=(betag*lambda)/4+dzo;
        Phis=Phis+M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/4-dzo;
        Lc=(betag*lambda)/2;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift_split(split*Le,Le,R,0);
        DeltaPhi=gap_split(split,E0TL, Phis, R, p, betas, To, kTo, k2To);
        drift_split(split*Ls, Ls,R,0);
    }
    if (m==2)
    {
        Le=(betag*lambda)/4+dzo+dzi;
        Ls=(betag*lambda)/2-dzi;
        Lc=3*(betag*lambda)/4;
        length=Lc;
        E0TL=E0T*(1+kE0Ti)*(Ti/Ts)*(Ls+Le);

        drift_split(split*Le,Le,R,0);
        DeltaPhi=gap_split(split,E0TL, Phis, R, p, betas, Ti, kTi, k2Ti);
        drift_split(split*Ls,Ls,R,0);

        for (cell=1;cell<n-1;cell++)
        {
            Le=1/4*betag*lambda;
            Phis=Phis+2*M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
            Ls=Le;
            Lc=betag*lambda;
            length=length+Lc;
            E0TL=E0T*(Ls+Le);

            drift_split(split*Le,Le,R,0);
            DeltaPhi=gap_split(split,E0TL, Phis, R, p, betas, Ts, kTs, k2Ts);
            drift_split(split*Ls,Ls,R,0);
        }

        Le=(betag*lambda)/2+dzo;
        Phis=Phis+2*M_PI+2*M_PI*(Ls+Le)/(lambda*relativistic_beta)+DeltaPhi;
        Ls=(betag*lambda)/4-dzo;
        Lc=3*(betag*lambda)/4;
        length=length+Lc;
        E0TL=E0T*(1+kE0To)*(To/Ts)*(Ls+Le);

        drift_split(split*Le,Le,R,0);
        DeltaPhi=gap_split(split,E0TL, Phis, R, p, betas, To, kTo, k2To);
        drift_split(split*Ls,Ls,R,0);
    }
    return length;
}

void multipole( double Order, double L, double step, double B, double R, double Lsol, double Zstep)
{
    int i,j;
    for (i=0;i<6;i++)
        for (j=0;j<6;j++)
            matrix[i][j]=0;

    for (i=0;i<6;i++)
        matrix[i][i]=1;

    matrix[0][1]=L;
    matrix[2][3]=L;
    matrix[4][5]=L/pow(relativistic_gamma,2);
    aperture=R;
    return;
}
void multipole_split(int split, double Order, double L, double step, double B, double R, double Lsol, double Zstep)
{
    multipole(Order,L,step,B,R,Lsol,Zstep);
    end_position=start_position;
    print_matrix();
    element_counter++;
    start_position=end_position;
    return;
}

void tracewin_parser(char *buf)
{
    int i;
    char *sep = " \t";
    char *field;

    for (i=0; buf[i]; i++)
        buf[i]=toupper(buf[i]);

    element.type=0;
    i=0;

    for (field = strtok(buf, sep); field; field = strtok(NULL, sep))
    {
        if (field[0]==';')
            return;
        else
        {
            if (element.type!=0)
            {
                element.param[i]=atof(field);
                i++;
            }
            if (strcmp(field,"DRIFT")==0)
                element.type=DRIFT;
            if (strcmp(field,"QUAD")==0)
                element.type=QUAD;
            if (strcmp(field,"EDGE")==0)
                element.type=EDGE;
            if (strcmp(field,"BEND")==0)
                element.type=BEND;
            if (strcmp(field,"DTL_CEL")==0)
                element.type=DTL_CEL;
            if (strcmp(field,"NCELLS")==0)
                element.type=NCELLS;
            if (strcmp(field,"MULTIPOLE")==0)
                element.type=MULTIPOLE;
            if (strcmp(field,"FREQ")==0)
                element.type=FREQ;
            if (strcmp(field,"GAP")==0)
                element.type=GAP;
            if (strncmp(field,"END",3)==0)
                element.type=END;
            /*
               if (element.type==0)
               printf("Element unknown:%s\n", field);
               */
        }
    }
    return;
}

int main(int argc, char **argv)
{
    char tracewin_string[1024];
    FILE *tracewin_file;
    int split;
    double geom[10];
    double length;
    int i;

    energy=3.0;
    frequency=352.21e6; /* For ESS */
    /* frequency=402.5e6; */ /* For SNS */
    relativistic_gamma=energy2gamma(energy);
    relativistic_beta=gamma2beta(relativistic_gamma);
    element_counter=1;

    if ((tracewin_file=fopen(argv[1],"r"))==NULL || argc != 3)
    {
        printf ("You must provide a tracewin file and a number of slices per meter\n");
        return -1;
    }
    split=2*atoi(argv[2]);

    file_matrix=fopen("matrices","w");
    file_energy=fopen("aperture-energy","w");
    file_geometry=fopen("geometry","w");

    start_position=0;
    end_position=0;

    while(fgets(tracewin_string, 1024, tracewin_file) != NULL && element.type!=END)
    {
        tracewin_parser(tracewin_string);
        start_position=end_position;
        for (i=0;i<10;i++)
            geom[i]=0;
        geom[0]=start_position;

        if (element.type==DRIFT)
        {
            drift_split(split*element.param[0]/1000,element.param[0]/1000, element.param[1]/1000, element.param[2]/1000);
            length=element.param[0]/1000;
            geom[1]=5;
        }
        if (element.type==QUAD)
        {
            quad_split(split*element.param[0]/1000,element.param[0]/1000, element.param[1], element.param[2]/1000, element.param[3], element.param[4], element.param[5], element.param[6], element.param[7]);
            length=element.param[0]/1000;
            if (element.param[1]>=0)
                geom[2]=5;
            else
                geom[2]=-5;
        }
        if (element.type==EDGE)
            edge_split(split,element.param[0]/180*M_PI, element.param[1]/1000, element.param[2]/1000, element.param[3], element.param[4], element.param[5]/1000, element.param[6]);
        if (element.type==BEND)
            bend_split(split*fabs(element.param[0]/180*M_PI),element.param[0]/180*M_PI, element.param[1]/1000, element.param[2], element.param[3]/1000, element.param[4]);
        if (element.type==DTL_CEL)
        {
            dtl_cell_split(split*element.param[0]/1000,element.param[0]/1000, element.param[1]/1000, element.param[2]/1000, element.param[3]/1000, element.param[4], element.param[5], element.param[6], element.param[7]/180*M_PI, element.param[8]/1000, element.param[9], element.param[10], element.param[11], element.param[12], element.param[13]);
            length=element.param[0]/1000;
            geom[3]=5;
        }
        if (element.type==NCELLS)
        {
            length=ncells_split(split,element.param[0], element.param[1], element.param[2], element.param[3], element.param[4]/180*M_PI, element.param[5]/1000, element.param[6], element.param[7], element.param[8], element.param[9]/1000, element.param[10]/1000, element.param[11], element.param[12], element.param[13], element.param[14], element.param[15], element.param[16], element.param[17], element.param[18], element.param[19], element.param[20]);
            geom[4]=5;
        }
        if (element.type==MULTIPOLE)
        {
            multipole_split(split*element.param[1]/1000,element.param[0], element.param[1]/1000, element.param[2], element.param[3], element.param[4]/1000, element.param[5]/1000, element.param[6]);
            length=element.param[1]/1000;
            geom[5]=5;
        }
        if (element.type==FREQ)
            frequency=element.param[0]*1000000;
        if (element.type==GAP)
        {
            if (split==0)
                gap(element.param[0], element.param[1]/180*M_PI, element.param[2]/1000, element.param[3], element.param[4], element.param[5], element.param[6], element.param[7]);
            else
                gap_split(split,element.param[0], element.param[1]/180*M_PI, element.param[2]/1000, element.param[3], element.param[4], element.param[5], element.param[6], element.param[7]);
        }

        for (i=0;i<9;i++)
            fprintf(file_geometry,"%f ",geom[i]);
        fprintf(file_geometry,"%f\n",geom[9]);

        geom[0]=geom[0]+length;
        for (i=0;i<9;i++)
            fprintf(file_geometry,"%f ",geom[i]);
        fprintf(file_geometry,"%f\n",geom[9]);
    }

    fclose(tracewin_file);
    fclose(file_matrix);
    fclose(file_energy);
    fclose(file_geometry);
    return 0;
}
